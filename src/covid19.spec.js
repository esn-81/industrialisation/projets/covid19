const {addCase, getAgeDistribution} = require('./covid19.js')
const Personne = require("./Models/Personne.js")

describe('addCase', () => {
    it("Ajout d'un nouveau cas", () => {
        var personne = new Personne("Steven", 29);
        const existingList = [];
        expect(addCase(existingList,personne)).toEqual([personne])
        expect(existingList).toEqual([]);
    })
})

describe('getAgeDistribution', () => {
    it("Récupération nombre de cas par age", () => {
        const cases = [
            { name: 'John Doe', age: 39 },
            { name: 'James Dean', age: 42 },
            { name: 'Billy Joel', age: 42 },
        ]

        const distribution = JSON.stringify({ 39: 1, 42: 2 });
        expect(getAgeDistribution(cases)).toEqual(distribution)
    })
})
