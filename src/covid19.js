const Personne = require("./Models/Personne.js");
const tabCases = []

function addCase(tabCases, Personne) {
    // Object.assign(target, source) permet de faire une copie du tableau 
    // afin d'éviter toute mutation du tableau source
    const newTabCase = Object.assign([], tabCases);
    newTabCase.push(Personne);  
    return newTabCase;   
}

function getAgeDistribution(CasesArray) {
    ageArray = {}
    CasesArray.forEach(element => {
        if (ageArray[element.age] != null) {
            ageArray[element.age] += 1;
        } else {
            ageArray[element.age] = 1;
        }
    });

    return JSON.stringify(ageArray);
}

module.exports = {addCase,getAgeDistribution}
